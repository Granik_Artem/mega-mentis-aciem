package webproject.MMA.service;

import webproject.MMA.entities.User;

public interface RegistrationService {
    void register(User NewUser);

    User findByUsername(String Username);
}
