package webproject.MMA.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import webproject.MMA.entities.User;
import webproject.MMA.repo.UserRepository;


@Service
public class RegistrationServiceImpl implements RegistrationService{
    @Autowired
    UserRepository userRepo;

    @Override
    public void register(User NewUser) {
        userRepo.save(NewUser);
    }

    @Override
    public User findByUsername(String Username) {
        return userRepo.findByUsername(Username);
    }

}
