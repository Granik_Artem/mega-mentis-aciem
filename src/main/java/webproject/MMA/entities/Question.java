package webproject.MMA.entities;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Entity
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "qp_id", nullable = false)
    private Long questionPacketId;
    @Column(name = "text", nullable = false)
    private String text;
    @Column(name = "correct_answer", nullable = false)
    private String correctAnswer;
    @Column(name = "wa_count", nullable = false)
    private int wrongAnswerCount;
    @Column(name = "wa1")
    private String wrongAnswer1;
    @Column(name = "wa2")
    private String wrongAnswer2;
    @Column(name = "wa3")
    private String wrongAnswer3;
    @Column(name = "wa4")
    private String wrongAnswer4;
    @Transient
    private List<String> answers;

    public Question(Long questionPacketId,
                    String text,
                    String correctAnswer,
                    int wrongAnswerCount,
                    String wrongAnswer1,
                    String wrongAnswer2,
                    String wrongAnswer3,
                    String wrongAnswer4) {
        this.questionPacketId = questionPacketId;
        this.text = text;
        this.correctAnswer = correctAnswer;
        this.wrongAnswerCount = wrongAnswerCount;
        this.wrongAnswer1 = wrongAnswer1;
        this.wrongAnswer2 = wrongAnswer2;
        this.wrongAnswer3 = wrongAnswer3;
        this.wrongAnswer4 = wrongAnswer4;
        this.answers = new ArrayList<>();
    }
    public void updateAnswers(){
        this.answers.add(this.correctAnswer);
        if(this.wrongAnswer1 != null) this.answers.add(this.wrongAnswer1);
        if(this.wrongAnswer2 != null) this.answers.add(this.wrongAnswer2);
        if(this.wrongAnswer3 != null) this.answers.add(this.wrongAnswer3);
        if(this.wrongAnswer4 != null) this.answers.add(this.wrongAnswer4);
        Collections.shuffle(answers);
    }
    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    public Question(Long questionPacketId,
                    String text,
                    String correctAnswer){
        this.questionPacketId = questionPacketId;
        this.text = text;
        this.correctAnswer = correctAnswer;
        this.wrongAnswerCount = 0;
        this.wrongAnswer1 = null;
        this.wrongAnswer2 = null;
        this.wrongAnswer3 = null;
        this.wrongAnswer4 = null;
        this.answers = new ArrayList<>();
    }

    public Question() {
        this.answers = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuestionPacketId() {
        return questionPacketId;
    }

    public void setQuestionPacketId(Long questionPacketId) {
        this.questionPacketId = questionPacketId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public int getWrongAnswerCount() {
        return wrongAnswerCount;
    }

    public void setWrongAnswerCount(int wrongAnswerCount) {
        this.wrongAnswerCount = wrongAnswerCount;
    }

    public String getWrongAnswer1() {
        return wrongAnswer1;
    }

    public void setWrongAnswer1(String wrongAnswer1) {
        this.wrongAnswer1 = wrongAnswer1;
    }

    public String getWrongAnswer2() {
        return wrongAnswer2;
    }

    public void setWrongAnswer2(String wrongAnswer2) {
        this.wrongAnswer2 = wrongAnswer2;
    }

    public String getWrongAnswer3() {
        return wrongAnswer3;
    }

    public void setWrongAnswer3(String wrongAnswer3) {
        this.wrongAnswer3 = wrongAnswer3;
    }

    public String getWrongAnswer4() {
        return wrongAnswer4;
    }

    public void setWrongAnswer4(String wrongAnswer4) {
        this.wrongAnswer4 = wrongAnswer4;
    }

}
