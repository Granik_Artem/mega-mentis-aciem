package webproject.MMA.entities;


import javax.persistence.*;

@Entity
public class QuestionPacket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "name", nullable = false, unique = true)
    private String name;
    @Column(name = "authorId", nullable = false)
    private Long authorId;
    public QuestionPacket(String name, Long authorId) {
        this.name = name;
        this.authorId = authorId;
    }
    public QuestionPacket(){
    }
    public Long getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
