package webproject.MMA.entities;


import javax.persistence.*;



@Entity(name = "User")
@Table(name ="USER")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "username",nullable = false, unique = true)
    private String username;
    @Column(name = "password",nullable = false)
    private String password;
    @Column(name = "rating")
    private Long rating;
    @Column(name = "status")
    private String status;

    public User(String username, String password){
        this.username = username;
        this.password = password;
        this.rating = Long.valueOf(0);
        this.status = "USER";
    }

    public User() {
        this.rating = Long.valueOf(0);
        this.status = "USER";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}