package webproject.MMA.entities;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
public class GameRoom {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "start_timestamp", nullable = false)
    private Date startTimestamp;
    @Column(name = "status", nullable = false)
    private String status;
    @Column(name = "player_count", nullable = false)
    private int playerCount;
    @Transient
    private int currentPlayers;
    @Transient
    private ArrayList<Long> questionPackets;
    @Column(name = "winner")
    private String Winner;

    public GameRoom(String name, int playerCount) {
        this.name = name;
        this.playerCount = playerCount;
        this.status = "IN_LOBBY";
        this.startTimestamp = new Date(System.currentTimeMillis());
        this.currentPlayers = 1;
        this.questionPackets = new ArrayList<>();
    }

    public GameRoom() {
        this.name = "placeholder";
        this.status = "IN_LOBBY";
        this.startTimestamp = new Date(System.currentTimeMillis());
        this.currentPlayers = 1;
        this.questionPackets = new ArrayList<>();
    }

    public void setStartTimestamp(Date startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public int getCurrentPlayers() {
        return currentPlayers;
    }

    public void setCurrentPlayers(int currentPlayers) {
        this.currentPlayers = currentPlayers;
    }

    public ArrayList<Long> getQuestionPackets() {
        return questionPackets;
    }

    public void setQuestionPackets(ArrayList<Long> questionPackets) {
        this.questionPackets = questionPackets;
    }

    public void addQuestionPackets(List<Long> questionPackets){
        questionPackets.addAll(questionPackets);
    }
    public String getWinner() {
        return Winner;
    }

    public void setWinner(String winner) {
        Winner = winner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public void setStatus(String status){
        this.status = status;

    }
    public void setPlayerCount(int playerCount){
        this.playerCount = playerCount;
    }
    public int getPlayerCount(){
        return playerCount;
    }
    public void setStartTimestamp(){
        this.startTimestamp = new Date(System.currentTimeMillis());
    }
    public Date getStartTimestamp(){
        return startTimestamp;
    }

    public String getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
