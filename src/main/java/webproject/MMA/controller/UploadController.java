package webproject.MMA.controller;


import com.opencsv.exceptions.CsvValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import webproject.MMA.repo.UserRepository;
import webproject.MMA.storage.StorageFileNotFoundException;
import webproject.MMA.storage.StorageService;
import webproject.MMA.utils.CSVUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.Principal;

@Controller
public class UploadController {
    private StorageService storageService;

    @Autowired
    public UploadController(StorageService storageService){
        this.storageService = storageService;
    }

    @Autowired
    UserRepository userRepo;


    @GetMapping("/upload")
    public String get(){
        return "upload";
    }

    @GetMapping("/upload/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @PostMapping("/upload")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   Model model, Principal principal) {
        if (file.isEmpty()) {
            model.addAttribute("message", "Не удалось сохранить пустой файл " + file.getOriginalFilename());
            return "/upload";
        }
        if(!file.getOriginalFilename().contains("csv")){
            model.addAttribute("message", "Неправильный формат файла");
            return "/upload";
        }
        if(file.getSize() > 10485760){
            model.addAttribute("message", "Файл слишком большой");
            return "/upload";
        }
        Long id = userRepo.findByUsername(principal.getName()).getId();
        storageService.store(file, id);
        boolean formatCheck = false;
        try {
           formatCheck = CSVUtils.CSVFormatCheck("src/main/resources/upload-dir/"+ id + " "  + file.getOriginalFilename());
        } catch (CsvValidationException | IOException e) {
            e.printStackTrace();
        }
        if(formatCheck) {
            model.addAttribute("message",
                    "Вы удачно загрузили файл " + file.getOriginalFilename() + "!");
        }else{
            File delete = new File("src/main/resources/upload-dir/" + id + " " + file.getOriginalFilename());
            try {
                Files.delete(delete.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            model.addAttribute("message",
                    "Ваш CSV файл не соответствует требуемому формату!");
        }
        return "/upload";
    }

    @ExceptionHandler({StorageFileNotFoundException.class})
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

}

