package webproject.MMA.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import webproject.MMA.entities.User;
import webproject.MMA.service.RegistrationService;

import java.util.Map;

@Controller
@RequestMapping("reg-page")
public class RegistrationController {
    @Autowired
    private RegistrationService regService;
    @Autowired
    BCryptPasswordEncoder passwordEncoder;
    @GetMapping
    public String registration_get(){
        return "reg-page";
    }
    @PostMapping
    public String registration_post(String username, String password, String repeat, Map<String, Object> model) {
        if (!password.equals(repeat)) {
            model.put("message", "Пароли не совпадают");
            return "reg-page";
        }

        final User userWithSameName = regService.findByUsername(username);
        if (userWithSameName != null) {
            model.put("message", "Такой пользователь уже есть");
            return "reg-page";
        }
        User user = new User(username, passwordEncoder.encode(password));
        regService.register(user);

        return "redirect:/login-page";
    }
}
