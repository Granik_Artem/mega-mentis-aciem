package webproject.MMA.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("main-page")
public class MainPageController {
    @RequestMapping
    public String get_page(){
        return "main-page";
    }
}
