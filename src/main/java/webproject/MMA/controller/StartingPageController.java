package webproject.MMA.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping({"starting-page"})
public class StartingPageController {
    @RequestMapping
    public String get_page(){
        return "starting-page";
    }

}


