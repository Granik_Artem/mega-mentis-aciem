package webproject.MMA.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import webproject.MMA.repo.QuestionPacketRepository;
import javax.servlet.http.HttpServletRequest;


@Controller
public class GameRoomsController {
    private int QPcount;
    @Autowired
    QuestionPacketRepository QPRepo;
    @GetMapping("creatingroom")
    public String creating_page(Model model){
        QPcount =  QPRepo.findAll().size();
        model.addAttribute("QPs", QPRepo.findAll());
        return "creatingroom";
    }
    @PostMapping("creatingroom")
    public String create_room(HttpServletRequest request, RedirectAttributes redirectAttributes){
        int y = 0;
        for (int i = 0; i < QPcount; i++) {
            if(request.getParameter(String.valueOf(i))!=null) {
                redirectAttributes.addAttribute("QP" + y, request.getParameter(String.valueOf(i)));
                y++;
            }
        }
        return "redirect:game";
    }
}
