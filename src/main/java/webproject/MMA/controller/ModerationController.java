package webproject.MMA.controller;


import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import webproject.MMA.entities.Question;
import webproject.MMA.entities.QuestionPacket;
import webproject.MMA.repo.QuestionPacketRepository;
import webproject.MMA.repo.QuestionRepository;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ModerationController {

    @Autowired
    QuestionRepository questionRepo;
    @Autowired
    QuestionPacketRepository questionPacketRepo;
    @RequestMapping("/moderation")
    public String get_page(Model model) throws IOException {
        List<String> QuestionPackets = Files.list(Paths.get("src/main/resources/upload-dir"))
                .map(e->e.getFileName().toString())
                .collect(Collectors.toList());
        List<Long> AuthorIds = QuestionPackets.stream().map(e->Long.valueOf(e.substring(0, e.indexOf(' ')))).collect(Collectors.toList());
        QuestionPackets = QuestionPackets.stream().map(e ->e.substring(e.indexOf(' ') + 1)).collect(Collectors.toList());
        model.addAttribute("Authors", AuthorIds);
        model.addAttribute("Names", QuestionPackets);
        return "moderation";
    }
    @GetMapping("/moderation/{qp}")
    public String get_question_packet_page(@PathVariable String qp, @RequestParam Long authorId, Model model) throws IOException, CsvException {
        String filename = "src/main/resources/upload-dir/" + authorId + " " + qp;
        CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
        CSVReader reader = new CSVReaderBuilder(new FileReader(filename)).withCSVParser(parser).withSkipLines(1).build();
        List<String[]> questions = reader.readAll();
        model.addAttribute("Questions", questions);
        model.addAttribute("AuthorId", authorId);
        reader.close();
        return "qp-preview";
    }
    @PostMapping("/moderation/{qp}")
    public String post_question_packet_page(@PathVariable String qp, @RequestParam Long authorId, @RequestParam boolean approved) throws IOException, CsvException {
        String filename = "src/main/resources/upload-dir/" + authorId + " " + qp;
        if(approved){
            CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
            CSVReader reader = new CSVReaderBuilder(new FileReader(filename)).withCSVParser(parser).withSkipLines(1).build();
            List<String[]> questions = reader.readAll();
            reader.close();
            questionPacketRepo.save(new QuestionPacket(qp.substring(0,qp.lastIndexOf('.')), authorId));
            Long qpId = questionPacketRepo.findByName(qp.substring(0,qp.lastIndexOf('.'))).getId();
            questions.stream().forEach(e -> questionRepo.save(new Question(qpId, e[0], e[1], Integer.valueOf(e[2]), e[3], e[4], e[5], e[6])));
        }
        Files.delete(Paths.get(filename));
        return "redirect:/moderation";
    }
}
