package webproject.MMA.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import webproject.MMA.repo.UserRepository;
import java.security.Principal;


@Controller
@RequestMapping("profile")
public class ProfilePageController {
    @Autowired
    private UserRepository userRepo;
    @GetMapping
    public String get_page(Model model, Principal principal){
        String username = principal.getName();
        model.addAttribute("user", userRepo.findByUsername(username));
        return "profile-page";
    }
}
