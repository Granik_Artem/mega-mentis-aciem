package webproject.MMA.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import webproject.MMA.repo.UserRepository;

@Controller
@RequestMapping("rating")
public class RatingController {
    @Autowired
    UserRepository userRepo;
    @GetMapping
    public String get_page(Model model){
        model.addAttribute("rating",userRepo.findAll(Sort.by(Sort.Direction.DESC, "Rating")));
        return "rate-page";
    }
}
