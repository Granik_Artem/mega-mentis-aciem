package webproject.MMA.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import webproject.MMA.entities.Question;
import webproject.MMA.entities.User;
import webproject.MMA.repo.QuestionRepository;
import webproject.MMA.repo.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Controller
public class GameController {
    @Autowired
    QuestionRepository questionRepo;
    @Autowired
    UserRepository userRepo;
    private int questionCounter = 0;
    private int correctAnswers = 0;
    boolean started = false;
    List<Question> Questions = new ArrayList<>();
    @GetMapping("game")
    public String get_page(HttpServletRequest request, Model model){
        List<Question> QuestionPool = new ArrayList<>();
        int i = 0;
        while(request.getParameter("QP" + i) != null){
            QuestionPool.addAll(questionRepo.findByQuestionPacketId(Long.valueOf(request.getParameter("QP" + i))));
            i++;
        }
        Random rand = new Random();
        int randomIndex;
        for (int j = 0; j < 10; j++) {
            randomIndex = rand.nextInt(QuestionPool.size());
            Question question = QuestionPool.remove(randomIndex);
            question.updateAnswers();
            Questions.add(question);
        }
        model.addAttribute("Started", started);
        started = true;
        return "game";
    }

    @PostMapping("/game/{question}")
    public String game_post(@PathVariable int question, Model model, String userAnswer){
        if(question < 10) {
            if(userAnswer != null) {
                if (userAnswer.equals(Questions.get(questionCounter-1).getCorrectAnswer())) {
                    correctAnswers++;
                }
            }
            questionCounter++;
            model.addAttribute("Started", started);
            model.addAttribute("Number", questionCounter);
            model.addAttribute("Question", Questions.get(question));
        }else{
            questionCounter++;
            model.addAttribute("Started", started);
            model.addAttribute("Number", questionCounter);
            model.addAttribute("CorrectAnswers", correctAnswers);
        }
        return "game";
    }
    @PostMapping("/game/finish")
    public String finish(Principal principal, @RequestParam int corrAnswers){
        User user = userRepo.findByUsername(principal.getName());
        Long rating = user.getRating() + 10 * corrAnswers;
        Long Id = user.getId();
        userRepo.setRatingById(rating,Id);
        questionCounter = 0;
        correctAnswers = 0;
        started = false;
        Questions = new ArrayList<>();
        return "redirect:/";
    }
}
