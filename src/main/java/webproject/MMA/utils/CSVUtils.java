package webproject.MMA.utils;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;

import java.io.FileReader;
import java.io.IOException;

public class CSVUtils {
    public static boolean CSVFormatCheck(String filename) throws IOException, CsvValidationException {
        CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
        CSVReader csvReader = new CSVReaderBuilder(new FileReader(filename)).withCSVParser(parser).build();
        String[] header = csvReader.readNext();
        boolean flag = (header[0].equalsIgnoreCase("text"))&&
                (header[1].equalsIgnoreCase("correct answer"))&&
                (header[2].equalsIgnoreCase("wa count"))&&
                (header[3].equalsIgnoreCase("wa 1"))&&
                (header[4].equalsIgnoreCase("wa 2"))&&
                (header[5].equalsIgnoreCase("wa 3"))&&
                (header[6].equalsIgnoreCase("wa 4"))&&
                (header.length == 7);
        csvReader.close();
        return flag;
    }
}
