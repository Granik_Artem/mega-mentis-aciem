package webproject.MMA.configuration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import webproject.MMA.repo.UserRepository;
import webproject.MMA.service.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserRepository userRepo;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/h2-console/**","/","/rating", "/css/**", "/images/**").permitAll()
                .antMatchers("/starting-page","/reg-page").anonymous()
                .antMatchers("/main-page").authenticated()
                .anyRequest().authenticated()
        .and()
                .formLogin()
                .loginPage("/login-page")
                .successForwardUrl("/main-page")
                .permitAll()
        .and()
                .logout().permitAll()
        .and()
                .rememberMe()
        .and()
                .httpBasic();
        http.csrf().disable();
        http.headers().frameOptions().disable();
    }
    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(new UserDetailsServiceImpl(userRepo));
    }

}
