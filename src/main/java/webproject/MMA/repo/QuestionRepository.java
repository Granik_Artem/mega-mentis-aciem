package webproject.MMA.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import webproject.MMA.entities.Question;

import java.util.List;

public interface QuestionRepository extends JpaRepository<Question, Long> {
    List<Question> findByQuestionPacketId(Long questionPacketId);
}
