package webproject.MMA.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import webproject.MMA.entities.QuestionPacket;

import java.util.List;

public interface QuestionPacketRepository extends JpaRepository<QuestionPacket, Long> {
    List<QuestionPacket> findByAuthorId(Long authorId);
    QuestionPacket findByName(String name);
}
