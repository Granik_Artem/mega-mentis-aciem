package webproject.MMA.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import webproject.MMA.entities.User;

import javax.transaction.Transactional;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    @Transactional
    @Modifying
    @Query("update User u set u.rating = ?1 where u.id = ?2")
    void setRatingById(Long rating, Long id);
}