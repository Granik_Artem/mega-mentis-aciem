package webproject.MMA.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import webproject.MMA.entities.GameRoom;

import java.util.List;

public interface GamesRepository extends JpaRepository<GameRoom, Long> {
    List<GameRoom> findAllByStatus(String status);
}
